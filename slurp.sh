## use system("sh /home/hans/src/slurp_twitter/slurp.sh &") in c to start this async
#mysql twitter -e "insert into new_users SELECT distinct tweets_temp.user_id from tweets_temp LEFT OUTER JOIN users ON tweets_temp.user_id = users.twitterid where users.twitterid is NULL; \

while [[ -a ~/mysql.write.lock ]] ; do
    sleep 5;
done
echo `date` "locking mysql by creating ~/mysql.write.lock" >> ~/slurp.log
touch ~/mysql.write.lock

mysql twitter -e "insert into users (twitterid, twittername, n_tweets, location, url, description, active_since) SELECT distinct user_id, user, n_tweets, user_location, user_url, user_description, active_since FROM tweets_temp ON DUPLICATE KEY UPDATE \`n_tweets\` = values(n_tweets), \`location\` = values(location), \`url\` = values(url), \`description\` = values(description), \`active_since\` = values(active_since); \
insert into sources (source) SELECT distinct tweets_temp.source FROM tweets_temp LEFT OUTER JOIN sources ON sources.source = tweets_temp.source where sources.id is NULL; \
insert into tweets_byid_partitioned SELECT id, text, datetime from tweets_temp; \
insert into tweets_by_inreplyto_partitioned SELECT id, in_reply_to from tweets_temp; \
insert into users_tweets SELECT random_id, text, tweets_temp.id, in_reply_to, sources.id as source, users.randomid as user, latitude, longitude, datetime FROM tweets_temp LEFT JOIN users ON tweets_temp.user = users.twittername LEFT JOIN sources ON sources.source = tweets_temp.source; \
delete from tweets_temp;"

echo `date` "unlocking mysql by removing ~/mysql.write.lock" >> ~/slurp.log
rm ~/mysql.write.lock

## create temporary table possible_dups select tweets_byid_partitioned.id from tweets_temp left join tweets_byid_partitioned partition (p8191) on tweets_byid_partitioned.id = tweets_temp.id; \
## delete from tweets_temp where id in (select id from possible_dups where not isnull(id)); \
## drop table possible_dups; \
