Summary
=======

slurp is a c program to consume the filter stream from twitter, publicly available at https://stream.twitter.com/1.1/statuses/filter.json

The program is based on https://github.com/sigabrt/slurp/blob/master/slurp.c and https://curl.haxx.se/libcurl/c/getinmemory.html and nxjson by Yaroslav Stavnichiy.

slurp is a fork of https://github.com/sigabrt/slurp. The rest of this readme is mostly copied from that project. The original slurp is a super simple C utility that, well, slurps down tweets from Twitter's sample stream. It was meant more as a proof-of-concept than anything else, so I would *not* recommend running this code in anything approaching a production scenario. However, hopefully it will be used to 1) give developers an idea of how to work with HTTP streams using libcurl (though again, not guaranteeing my approach is even close to the best one, copy at your own risk), and 2) help people who just want a simple way to download tons of raw Twitter data.

Differences compared to the original slurp
==========================================

* hardcoded a special filter string, which is based on language and latitude and longitude of the user, not keywords.
* improved processing of data: data is parsed and used to build a query, buffered in RAM until the query reaches a predefined threshold, currently 2MB, when it is saved to a relational database.

Building
========

libcurl, liboauth and libmariaclient are required.

On a debian system install the following packages:

* `libmariadbclient-dev`, `libcurl4-gnutls-dev`, `liboauth-dev`

Run `make`. 



Running
=======

`./slurp keys.txt mysql_credentials.txt`

`keys.txt` (or whatever you choose to call it) is a plain text file containing your Twitter OAuth app information, in this order:

* Consumer key
* Consumer secret
* Access token
* Access token secret

If you don't have this stuff, log into the Twitter dev center and create a new application.

`mysql_credentials.txt` (or whatever you choose to call it) is a plain text file containing credentials for mysql, and the host and the database to connect to, in this order:

* user
* password
* host
* database


Details
=======

slurp depends on `asprintf()` which is a GNU extension, see https://www.gnu.org/software/libc/manual/html_node/Dynamic-Output.html.