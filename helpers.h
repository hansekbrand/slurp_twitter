/* From the libc manual, always check if malloc() got the memory it requested */
void * xmalloc (size_t size) {
  void *value = malloc (size);
  if (value == 0) {
    printf("ERROR: Virtual memory exhausted! \n");
    /* exit(EXIT_FAILURE); */
    abort();
  } 
  return value;
}
