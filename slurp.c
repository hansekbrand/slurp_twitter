/* slurp.c is based on work made by Daniel Stenberg (getinmemory), Alexander Meyer (slurp), and Yaroslav Stavnichiy (nxjson) */

/* https://curl.haxx.se/libcurl/c/getinmemory.html */
/***************************************************************************
 *                                  _   _ ____  _
 *  Project                     ___| | | |  _ \| |
 *                             / __| | | | |_) | |
 *                            | (__| |_| |  _ <| |___
 *                             \___|\___/|_| \_\_____|
 *
 * Copyright (C) 1998 - 2015, Daniel Stenberg, <daniel@haxx.se>, et al.
 *
 * This software is licensed as described in the file COPYING, which you should have received as part of this distribution. The terms are also available at https:curl.haxx.se/docs/copyright.html.
 *
 * You may opt to use, copy, modify, merge, publish, distribute and/or sell copies of the Software, and permit persons to whom the Software is furnished to do so, under the terms of the COPYING file.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *
 ***************************************************************************/
/* <DESC> */
/* Shows how the write callback function can be used to download data into a */
/* chunk of memory instead of storing it in a file. */
/* </DESC> */

/* Alexander Meyer */
/* https://github.com/sigabrt/slurp/blob/master/slurp.c   */
/* Public domain, do what you want with it, though I'd certainly be interested to hear what you're using it for. :) */

/* Yaroslav Stavnichiy (nxjson) */
/* nxjson.h */
/* nxjson.c */
/* NXJSON is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. */

#define _GNU_SOURCE /* asprintf() */
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* asprintf() */
#include <time.h> /* strftime() */
#include <unistd.h> /* getopt() */
#include <oauth.h>
#include <curl/curl.h>
#include "nxjson.h"
#include "helpers.h" /* xmalloc() from libc documentation */
#include <mariadb/mysql.h>

#define MS_TO_NS (1000000)

#define DATA_TIMEOUT (90)
#define MAX_KEYWORDS (16)

/* Accumulate data until the query gets this big, then execute the query and free() it */
/* #define MIN_QUERY_LENGTH (3000) */
/* #define MIN_QUERY_LENGTH (2000000) */
int MIN_QUERY_LENGTH = 3000;
/* Remember, the query must not be larger than max_allowed_packet. I use /etc/mysql/mariadb.conf.d/50-server.cnf:max_allowed_packet = 16M */
#define QUERY_BUFFER_INCREASE_STEP_SIZE (50000)

#define MAX_ROW_LENGTH (20000) /* when the next newline character is this far away from the current position, then we assume something is wrong and ignore the chunk. Normal row length is 2000-5000. */

/* Define the global variables */
char * mysql_secrets_file_name;
char * my_error_log_file_name = "slurp-error.log";
char * my_great_query;
#define init_string_for_table_tweets "INSERT IGNORE INTO tweets_temp (random_id, text, id, in_reply_to, source, user, user_id, latitude, longitude, datetime, n_tweets, active_since, user_location, user_description, user_url) VALUES "
const int nchar_init_string = strlen(init_string_for_table_tweets);
long unsigned int my_great_query_allocated_size;
long unsigned int cursor_in_great_query_buffer;
int row_counter = 0;

/* write callback by Stenberg START */
struct MemoryStruct {
  char *memory;
  size_t size;
};
/* write callback by Stenberg STOP */

/* Meyer START */
/* read_auth_keys
 * filename: The name of the text file containing
 * 		the keys
 * bufsize: The maximum number of characters to read per line
 * ckey: Consumer key, must be allocated already
 * csecret: Consumer secret, must be allocated already
 * atok: App token, must be allocated already
 * atoksecret: App token secret, must be allocated already
 */
void read_auth_keys(const char *filename, int bufsize,
		char *ckey, char *csecret, char *atok, char *atoksecret)
{
	FILE *file = fopen(filename, "r");

	if(file == NULL){
	  printf("Failed to open file: %s\n", filename);
	  exit(EXIT_FAILURE);
	}

	if(fgets(ckey, bufsize, file) == NULL)
	{
		return;
	}
	ckey[strlen(ckey)-1] = '\0'; // Remove the newline

	if(fgets(csecret, bufsize, file) == NULL)
	{
		return;
	}
	csecret[strlen(csecret)-1] = '\0';

	if(fgets(atok, bufsize, file) == NULL)
	{
		return;
	}
	atok[strlen(atok)-1] = '\0';

	if(fgets(atoksecret, bufsize, file) == NULL)
	{
		return;
	}
	atoksecret[strlen(atoksecret)-1] = '\0';

	fclose(file);
}
/* Meyer STOP */

void print_error_to_console(const char *error) {
  char timestr[20];
  time_t t = time(NULL);
  struct tm *tmp = localtime(&t);
  strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", tmp);
  fprintf(stderr, "%s %s", timestr, error);
}

void process_data(struct MemoryStruct * mem, long unsigned int cut_here) {
  /* It is somewhat ugly to re-read the ini-file each time we process data, but on the other hand: this way we can change password without stoping and starting the program, which is kind of cool. */
  /* In addition, if we read this file only in main, then we would either have to use global variables, or pass the variables as arguments to the callback function, which seems rather complicated. */

  /* Since argv is not defined yet, we rely on a global variable for the filename */

  const int bufsize = 64;
  char *my_user, *my_passwd, *my_host, *my_database;
  my_user = (char *)malloc(bufsize * sizeof(char));
  my_passwd = (char *)malloc(bufsize * sizeof(char));
  my_host = (char *)malloc(bufsize * sizeof(char));
  my_database = (char *)malloc(bufsize * sizeof(char));

  /* We reuse read_auth_keys() which was originally written for the four elements needed for oauth */
  read_auth_keys(mysql_secrets_file_name, bufsize, my_user, my_passwd, my_host, my_database);
  if(my_user == NULL || my_passwd == NULL || my_host == NULL || my_database == NULL) {
      fprintf(stderr, "Couldn't read mysql secrets file. Aborting...\n");
      free(my_user);
      free(my_passwd);
      free(my_host);
      free(my_database);
      exit(EXIT_FAILURE);
  }

  /* We need a mysql_handle in order to get access to the function mysql_real_escape_string(). */
  MYSQL * mysql_handle = mysql_real_connect(mysql_init(NULL),
					    my_host,
					    my_user,
					    my_passwd,
					    my_database,
					    0,
					    "/var/run/mysqld/mysqld.sock",
					    CLIENT_MULTI_STATEMENTS);
  if(mysql_handle == NULL){
    printf("Failed to create a mysql_handle, is the server running?\n");
    exit(EXIT_FAILURE);
  }
  int my_set_charset_results = mysql_set_character_set(mysql_handle, "utf8mb4");
  if(my_set_charset_results > 0){
    printf("Failed to set the character set of the connection to utf8mb4\n");
    exit(EXIT_FAILURE);
  }

  /* Allocate a buffer for the query */
  /* to optimise speed we start with a reasonably large buffer, that most often will be enough */  
  /* char * my_query = xmalloc (cut_here/20); */
  /* long unsigned int my_query_allocated_size = cut_here/20; */
  /* Define the start of the query string */
  /* memcpy(my_query, "INSERT INTO tweets (text, id, in_reply_to, source, user, latitude, longitude, datetime, sample) VALUES ", 103); */
  /* append new rows at this position in my_query */  
  /* long unsigned int cursor_in_query_buffer = 103; */
  
  /* int sample_is_geo = 0; /\* Flag for if there is geo data or not *\/ */
  char * line = xmalloc (MAX_ROW_LENGTH); /* Maximum size of the json record of a tweet. We don't know, but let's guess that 20K chars is more than enough */
  /* chunks larger than this will be ignored, which is good because there is problably something wrong there */
  long unsigned int checked_for_newlines_upto_here = 0; /* For keeping track of the part of the buffer that is already processed */
  int n_bytes_to_next_newline = 0; /* next newline in the input buffer, counting from checked_for_newlines_upto_here */
  
  /* cut_here is the size of the input buffer */
  while(checked_for_newlines_upto_here < cut_here){
    n_bytes_to_next_newline = strchr(mem->memory + checked_for_newlines_upto_here, '\n') - (mem->memory + checked_for_newlines_upto_here); 
    if(strchr(mem->memory + checked_for_newlines_upto_here, '\n') == NULL){
      /* strchr() returns NULL if there are no more newlines */
      printf("no (more) newlines in mem->memory which contains: %s \n", mem->memory);
      break; /* We are done */
    } 
    /* Occasionally, there are two or three consequtive newlines, test that we have read more than 3 bytes and that our line-buffer is big enough. */
    if((n_bytes_to_next_newline > 3) & (n_bytes_to_next_newline < MAX_ROW_LENGTH)){
      /* Found a json record */
    
      /* Fill line with data from the current line/json record */
      /* Occasionally, here is a glitch in the matrix and the line does not start with "{" */
      /* For example I once saw this error: */
      /* NXJSON PARSE ERROR (141): invalid unicode escape at \u00{"created_at" */
      /* So, we must disregard any characters before the first "{" */
      strncpy(line, mem->memory + checked_for_newlines_upto_here, n_bytes_to_next_newline);
      line[n_bytes_to_next_newline + 1] = '\0'; /* Add a terminating NULL */
      /* Parse the current line/json record */
      /* printf("line is %s\n", line); */
	
      const nx_json* json=nx_json_parse_utf8(line);
      if(json){
	/* The json record was valid. Why would it not be valid? Well, we cut the buffer at newlines, and the json data can contain binary parts, e.g. image data, and I think there can be newline characters in those binary parts. */
	/* Only include tweets in swedish */
	if(strcmp(nx_json_get(json, "lang")->text_value, "sv") == 0) { /* The tweet is written in Swedish */
	  row_counter = row_counter + 1; /* Count the effective number of swedish tweets */
	  double lat = 0;
	  double lon = 0;
	  /* TODO: use place->boundingbox->array(coordinates) instead, since it has data even when geo is null */
	  if(nx_json_get(json, "geo")->type == NX_JSON_OBJECT){ /* There should geo data in it */
	    if(nx_json_get(nx_json_get(json, "geo"), "coordinates")->type == NX_JSON_ARRAY){ /* There are coordinates here */
	      const nx_json* arr=nx_json_get(nx_json_get(json, "geo"), "coordinates");
	      lat = nx_json_item(arr, 0)->dbl_value;
	      lon = nx_json_item(arr, 1)->dbl_value;
	      /* sample_is_geo = 1; */
	    }
	  }
	  size_t str_len = strlen(nx_json_get(json, "text")->text_value);
	  char sanitized_tweet [str_len * 2 + 1]; /* To double the size is really just a guess about what the sanitization will output  */
	  int sanitization_tweet_results = mysql_real_escape_string(mysql_handle,
								    sanitized_tweet,
								    nx_json_get(json, "text")->text_value,
								    str_len);
	    if(sanitization_tweet_results < 1){
	      printf("Failed to sanitize text %s\n", nx_json_get(json, "text")->text_value);
	      exit(EXIT_FAILURE);
	    }
	    str_len = strlen(nx_json_get(json, "source")->text_value);
	    char sanitized_source [str_len * 2 + 1];
	    int sanitization_source_results = mysql_real_escape_string(mysql_handle,
								       sanitized_source,
								       nx_json_get(json, "source")->text_value,
								       str_len);
	    if(sanitization_source_results < 1){
	      printf("Failed to sanitize source %s\n", nx_json_get(json, "source")->text_value);
	      exit(EXIT_FAILURE);
	    }
	    /* Start with user screen name */
	    /* be robust if 'screen_name' is lacking from 'user'. Not really expected for 'screen_name' so this is more of prof-of-concept for other attributes */
	    str_len = 1;
	    if(nx_json_get(nx_json_get(json, "user"), "screen_name")->type == NX_JSON_STRING){
	      str_len = strlen(nx_json_get(nx_json_get(json, "user"), "screen_name")->text_value);
	    }
	    char sanitized_screen_name [str_len * 2 + 1];
	    if(nx_json_get(nx_json_get(json, "user"), "screen_name")->type == NX_JSON_STRING){	    
	      int sanitization_screen_name_results = mysql_real_escape_string(mysql_handle,
									      sanitized_screen_name,
									      nx_json_get(nx_json_get(json, "user"), "screen_name")->text_value,
									      str_len);
	      if(sanitization_screen_name_results < 1){
		printf("Failed to sanitize %s\n", nx_json_get(nx_json_get(json, "user"), "screen_name")->text_value);
		exit(EXIT_FAILURE);
	      }
	    } else {
	      strcpy(sanitized_screen_name, "NULL");
	    }
	    /* Finished with user screen name */
	    /* Start with user location */
	    str_len = 1;
	    if(nx_json_get(nx_json_get(json, "user"), "location")->type == NX_JSON_STRING){
	      str_len = strlen(nx_json_get(nx_json_get(json, "user"), "location")->text_value);
	    }
	    char sanitized_location [str_len * 2 + 1];
	    if(nx_json_get(nx_json_get(json, "user"), "location")->type == NX_JSON_STRING){	    
	      int sanitization_location_results = mysql_real_escape_string(mysql_handle,
									   sanitized_location,
									   nx_json_get(nx_json_get(json, "user"), "location")->text_value,
									   str_len);
	      if(sanitization_location_results < 1){
		printf("Failed to sanitize %s\n", nx_json_get(nx_json_get(json, "user"), "location")->text_value);
		exit(EXIT_FAILURE);
	      }
	    } else {
	      strcpy(sanitized_location, "NULL");
	    }
	    /* Start with user description */
	    str_len = 1;	    
	    if(nx_json_get(nx_json_get(json, "user"), "description")->type == NX_JSON_STRING){
	      str_len = strlen(nx_json_get(nx_json_get(json, "user"), "description")->text_value);
	    }
	    char sanitized_description [str_len * 2 + 1];
	    if(nx_json_get(nx_json_get(json, "user"), "description")->type == NX_JSON_STRING){
	      int sanitization_description_results = mysql_real_escape_string(mysql_handle,
									      sanitized_description,
									      nx_json_get(nx_json_get(json, "user"), "description")->text_value,
									      str_len);
	      if(sanitization_description_results < 1){
		printf("Failed to sanitize %s\n", nx_json_get(nx_json_get(json, "user"), "description")->text_value);
		exit(EXIT_FAILURE);
	      }
	    } else {
	      strcpy(sanitized_description, "NULL");	      
	    }
	    /* Start with user url */
	    str_len = 1;
	    if(nx_json_get(nx_json_get(json, "user"), "url")->type == NX_JSON_STRING){
	      str_len = strlen(nx_json_get(nx_json_get(json, "user"), "url")->text_value);
	    }
	    char sanitized_user_url [str_len * 2 + 1];
	    if(nx_json_get(nx_json_get(json, "user"), "url")->type == NX_JSON_STRING){	    
	      int sanitization_user_url_results = mysql_real_escape_string(mysql_handle,
									   sanitized_user_url,
									   nx_json_get(nx_json_get(json, "user"), "url")->text_value,
									   str_len);
	      if(sanitization_user_url_results < 1){
		printf("Failed to sanitize %s\n", nx_json_get(nx_json_get(json, "user"), "url")->text_value);
		exit(EXIT_FAILURE);
	      }
	    } else {
	      strcpy(sanitized_user_url, "NULL");	      
	    }
	    
	    /* For all but the first row, add a "," between the rows. */
	    char * row_str_prefix = ""; /* This one is small, we don't need to xmalloc() it, it can live in the stack. */
	    if (row_counter > 1){
	      row_str_prefix = ", ";
	    }
	    /* Make a proper "NULL" for the SQL query */
	    char sanitized_in_reply_to [20];
	    if(!nx_json_get(json, "in_reply_to_status_id_str")->text_value){
	      strcpy(sanitized_in_reply_to, "NULL");
	      /* The tweet is not a reply */
	    } else {
	      /* This one is a reply */	    
	      /* Currently tweetid is a long integer, so there is currently no need to sanitize this data, but we do it anyway */
	      strcpy(sanitized_in_reply_to, nx_json_get(json, "in_reply_to_status_id_str")->text_value);
	    }
	    
	    /* Define the format used by twitter */
	    const char *myfmt = "%A %B %d %H:%M:%S %z %Y";
	    /* Create a fully initialised time object */
	    time_t rawtime;
	    time (&rawtime);
	    struct tm *foo = localtime(&rawtime);
	    /* Parse the string according the rules in myfmt and fill the time object foo with data */
	    strptime(nx_json_get(json, "created_at")->text_value, myfmt, foo);
	    /* Initialise the target variable mytime */
	    char mytime [20];
	    /* Write the time in the format required by SQL to mytime */
	    strftime(mytime, 20, "%Y-%m-%d %H:%M:%S", foo);
	    /* Repeat for the time when the user opened the account */
	    char active_since [20];
	    struct tm *foo_two = localtime(&rawtime);	    
	    strptime(nx_json_get(nx_json_get(json, "user"), "created_at")->text_value, myfmt, foo_two);
	    strftime(active_since, 20, "%Y-%m-%d %H:%M:%S", foo_two);
	    /* printf("time-stamp parsed into %s\n", mytime); */
	    /* Ideally, we should pass id as an integer, but it is just too much hassle, since twitter sends it as a string in json */
	    const char * id_str = nx_json_get(json, "id_str")->text_value;
	    /* "id" and uint64_t aka long long is safe to use according to twitter, "id_str" is the same data enclosed in quotation marks */
	    const long long int user_id = nx_json_get(nx_json_get(json, "user"), "id")->int_value;
	    const long long int n_tweets = nx_json_get(nx_json_get(json, "user"), "statuses_count")->int_value; /* int value is of type long long which is at least 64 bits and should suffice  */
	    /* Create the row of values */
	    char * row_str;
	    /* Below is the one and only place where we use asprintf(), perhaps we could do without it */
	    int size = asprintf(&row_str, "%s(%i, '%s', '%s', '%s', '%s', '%s', %lli, %f, %f, '%s', '%lli', '%s', '%s', '%s', '%s')",
				row_str_prefix,
				(rand() % 8192) - 1,/* Random integer between 0 and 8191 */
				sanitized_tweet,
				id_str,
				sanitized_in_reply_to,
				sanitized_source,
				sanitized_screen_name,
				user_id,
				lat,
				lon,
				mytime,
				n_tweets, /* meta data about the user */
				active_since,
				sanitized_location,
				sanitized_description,
				sanitized_user_url
				);
	    
	    if(size == -1 ) {
	      printf("failed at creating a proper row_str asprintf\n");
	      exit(EXIT_FAILURE);	    
	    }

	    /* Append this row of values to our query in a way that does not leak memory, that is do not use asprintf() repeatedly */
	    /* Check if my_great_query has room for row_str AND a ";" AND a terminating NULL */
	    /* my_great_query_allocated_size = QUERY_BUFFER_INCREASE_STEP_SIZE;	 */
	    while((cursor_in_great_query_buffer + size + 2) > my_great_query_allocated_size){
	      /* my_great_query does not have room for row_str */
	      my_great_query = realloc(my_great_query, my_great_query_allocated_size + QUERY_BUFFER_INCREASE_STEP_SIZE);
	      if(my_great_query == NULL){
		printf("failed to increase the query buffer size from %lud to %lud\n", my_great_query_allocated_size, my_great_query_allocated_size + QUERY_BUFFER_INCREASE_STEP_SIZE);
		exit(EXIT_FAILURE);
	      } else {
		my_great_query_allocated_size = my_great_query_allocated_size + QUERY_BUFFER_INCREASE_STEP_SIZE;
	      }
	    }
	    /* printf("about to copy row_str %s onto my_great_query %s, copying %d bytes starting at %lud in my_great_query which is now %d bytes long\n", row_str, my_query, size, cursor_in_query_buffer, strlen(my_query)); */
	    /* copy:       Here in target,            source,   this much of source */

	    /* /\* I don't trust my old router, please let me know that you have got some tweets *\/ */
	    /* printf("I have got %d tweets, taking up %lu bytes, %lu bytes left before writing to db \n", row_counter, cursor_in_great_query_buffer, MIN_QUERY_LENGTH-cursor_in_great_query_buffer); */
	    
	    memcpy(my_great_query + cursor_in_great_query_buffer, row_str, size);
	    cursor_in_great_query_buffer = cursor_in_great_query_buffer + size;
	    /* printf("query is now %lud bytes long own calculation \n", cursor_in_great_query_buffer);	     */
	    /* row_counter++; */
	    free(row_str); /* Implicitly malloced() by asprintf() */
	}
	nx_json_free(json);
      }
    }
    checked_for_newlines_upto_here = checked_for_newlines_upto_here + n_bytes_to_next_newline + 1; /* Move the index counter n steps */
  }

  free(line);

  /* Is the query big enough to be executed?   */
  if(cursor_in_great_query_buffer > MIN_QUERY_LENGTH){
    /* Add a ";" and a terminating NULL to the end of the query */
    memcpy(my_great_query + cursor_in_great_query_buffer, ";\0", 2);
    
    int my_result = mysql_real_query(mysql_handle, my_great_query, strlen(my_great_query));
    /* int my_result = 1; */
    if(my_result != 0){
      /* print the failed query in the error log and move on */
      printf("Failure writing %d tweets to database! error %d \n", row_counter, my_result);
      /* Make the error log file name unique, which makes it easier to post-process the error file log. */
      /* Use the current time to make the error log file name unique */
      char error_log_timestr[20];
      time_t error_log_t = time(NULL);
      struct tm *error_log_tmp = localtime(&error_log_t);
      strftime(error_log_timestr, 20, "%Y-%m-%d %H:%M:%S", error_log_tmp);
      char * unique_error_log_file_name;
      int error_log_size = asprintf(&unique_error_log_file_name, "%s-%s", my_error_log_file_name, error_log_timestr);
      if(error_log_size == -1 ) {
	printf("failed at creating a proper unique_error_log_file_name asprintf\n");
	      exit(EXIT_FAILURE);	    
      }
      FILE * fh = fopen(unique_error_log_file_name, "w");
      printf("successfully opened file %s \n", unique_error_log_file_name);
      /* We have used the name, we can now free it (asprintf() implicitly uses alloc()) */
      free(unique_error_log_file_name);
      if(fh != NULL){
	/* Successfully opened the error log */
	fputs(my_great_query, fh);
	fclose(fh);
	/* Try to restart with a new query */	
	my_great_query = realloc(my_great_query, QUERY_BUFFER_INCREASE_STEP_SIZE);
	if(my_great_query == NULL){
	  /* We failed */
	  printf("Failed to release unused memory, giving up...\n");
	  exit(EXIT_FAILURE);
	}
      } else {
	printf("Failed to open error log, giving up...\n");
	exit(EXIT_FAILURE);
      }
    } else {
      /* Successfully stored the records in the db */
      char timestr[20];
      time_t t = time(NULL);
      struct tm *tmp = localtime(&t);
      strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", tmp);
      printf("%s successfully saved %d tweets \n", timestr, row_counter);
      my_great_query = realloc(my_great_query, QUERY_BUFFER_INCREASE_STEP_SIZE);
      if(my_great_query == NULL){
	/* We failed */
	printf("Failed to release unused memory, giving up...\n");
	exit(EXIT_FAILURE);
      }
    }
    /* Restart with a new query */
    cursor_in_great_query_buffer = nchar_init_string;
    my_great_query_allocated_size = QUERY_BUFFER_INCREASE_STEP_SIZE;
    row_counter = 0;

    /* Initiate some SQL stuff at the server */
    system("bash /home/hans/src/slurp_twitter/slurp.sh &");
  }

  mysql_close(mysql_handle);
  free(my_user);
  free(my_passwd);
  free(my_host);
  free(my_database);
  
  /* We are ok, return void */
  return;
}

/* write callback by Stenberg START */
static size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)userp;
  mem->memory = realloc(mem->memory, mem->size + realsize + 1);
  if(mem->memory == NULL) {
    /* out of memory! */
    printf("not enough memory (realloc returned NULL)\n");
    return 0;
  }
  
  /* Save data to our global buffer mem->memory */
  memcpy(&(mem->memory[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->memory[mem->size] = 0;

  /* This block by Hans Ekbrand START */
  /* Find the last newline character, cut buffer from its start to this position. Process that part to disk, and copy the rest of the buffer the top of the buffer, update mem->size.  */
  /* Find the last newline character by iterating backwards. */
  long unsigned int cut_here = 0;
  int i = mem->size-1;
  while(i > 0){
    if(mem->memory[i] == '\n'){
      cut_here = i;
      break;
    }
    i = i - 1;
  }
  /* Only try to process data if we found a newline */
  /* last newline character is at cut_here. */
  if(cut_here > 0){
    process_data(mem, cut_here); /* cut_here is not a pointer, but mem is */
    /* process_data() will abort if it fails, so no need to return a value */
    
    /* Copy the remaining character(s) to start of mem->memory */
    memcpy(mem->memory, &mem->memory[cut_here + 1], mem->size - (cut_here + 1));
    
    /* Update mem->size, the buffer is smaller now */
    mem->size = mem->size - (cut_here + 1);
    /* This block by Hans Ekbrand STOP */
    /* } */
  }
  return realsize;
}

/* write callback by Stenberg STOP */

/* This part by Meyer START */
typedef enum
{
	ERROR_TYPE_HTTP,
	ERROR_TYPE_RATE_LIMITED,
	ERROR_TYPE_SOCKET,
} error_type;

struct idletimer
{
	int lastdl;
	time_t idlestart;
};

void config_curlopts(CURL *curl, const char *url, char *postargs, void *prog_data);

void reconnect_wait(error_type error);
/* This part by Meyer STOP */

/* write callback by Stenberg START */
int progress_callback(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow);
struct MemoryStruct chunk;
/* write callback by Stenberg STOP */

int main(int argc, char *argv[])
{

  /* Check that argv is not NULL */
  if ((argv[2] == 0) | (argc == 1)){
    printf("This program requires two parameters.\n");
    exit(0);
  }

  /* Check for the third optional parameter (four with program name) */
  if(argc == 4){
    MIN_QUERY_LENGTH = atoi(argv[3]);
    printf("query length %i\n", MIN_QUERY_LENGTH);
  }

  my_great_query = malloc(QUERY_BUFFER_INCREASE_STEP_SIZE);
  my_great_query_allocated_size = QUERY_BUFFER_INCREASE_STEP_SIZE;
  /* Define the start of the query string */
  cursor_in_great_query_buffer = nchar_init_string; /* Global variable, append new rows at this position in my_query */
  memcpy(my_great_query, init_string_for_table_tweets, cursor_in_great_query_buffer);

  /* write callback by Stenberg START */
  chunk.memory = malloc(1);  /* will be grown as needed by the realloc above */
  chunk.size = 0;    /* no data at this point */
  /* write callback by Stenberg STOP */

  /* This part by Meyer START */  
  /* File containing the OAuth keys */
  const char *keyfile = NULL;

  /* OAuth keys */
  char *ckey, *csecret, *atok, *atoksecret;

  /* Full unsigned URL with parameters */
  char *url = NULL;

  /* Signed URL */
  char *signedurl = NULL;

  /* POST parameters */
  char *postargs = NULL;

  keyfile = argv[1];

  /* File containing the mysql username, password, host, and table */
  mysql_secrets_file_name = argv[2];

  static const char *myurl = "https://stream.twitter.com/1.1/statuses/filter.json?lang=sv&locations=11.13373,55.34008,24.15473,69.04941";
	
  printf("url: %s \n", myurl);

  // Read the OAuth keys
  // ===================
  // These may be found on your twitter dev page, under "Applications"
  // You will need to create a new app if you haven't already
  // The four keys should be on separate lines in this order:
  int bufsize = 64;
  ckey = (char *)malloc(bufsize * sizeof(char));
  csecret = (char *)malloc(bufsize * sizeof(char));
  atok = (char *)malloc(bufsize * sizeof(char));
  atoksecret = (char *)malloc(bufsize * sizeof(char));
  read_auth_keys(keyfile, bufsize, ckey, csecret, atok, atoksecret);
  if(ckey == NULL || csecret == NULL ||
     atok == NULL || atoksecret == NULL)
    {
      fprintf(stderr, "Couldn't read key file. Aborting...\n");
      free(ckey);
      free(csecret);
      free(atok);
      free(atoksecret);
      return 1;
    }


  // Sign the URL with OAuth
  // =======================
  // Note that all parameters will be stripped from the URL
  // and added to postargs automatically
  signedurl = oauth_sign_url2(myurl, &postargs, OA_HMAC, "POST", ckey, csecret, atok, atoksecret);

  // Initialize the idle timer
  // =========================
  struct idletimer timeout;
  timeout.lastdl = 1;
  timeout.idlestart = 0;

  // Initialize libcURL
  // ==================
  curl_global_init(CURL_GLOBAL_ALL);
  CURL *curl = curl_easy_init();
  config_curlopts(curl, signedurl, postargs, (void *)&timeout);

  int curlstatus, httpstatus;
  char reconnect = 1;

  while(reconnect)
    {
      curlstatus = curl_easy_perform(curl);
      switch(curlstatus)
	{
	case 0: // Twitter closed the connection
	  print_error_to_console("Connection terminated. Attempting reconnect...\n");
	  reconnect_wait(ERROR_TYPE_SOCKET);
				curl_easy_cleanup(curl);
				curl = curl_easy_init();
				
				// The signed URL contains a timestamp, so it needs to be
				// regenerated each time we reconnect or else we'll get a 401
				signedurl = oauth_sign_url2(myurl, &postargs, OA_HMAC, "POST", ckey, csecret, atok, atoksecret);
				config_curlopts(curl, signedurl, postargs, (void *)&timeout);
				break;
	case CURLE_HTTP_RETURNED_ERROR:
	  curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpstatus);
	  switch(httpstatus)
	    {
	    case 401:
	    case 403:
	    case 404:
	    case 406:
	    case 413:
	    case 416:
	      // No reconnects with these errors
	      fprintf(stderr, "Request failed with HTTP error %d. Aborting...\n", httpstatus);
	      reconnect = 0;
	      break;
	    case 420:
	      fprintf(stderr, "Received rate limiting response, attempting reconnect...\n");
	      reconnect_wait(ERROR_TYPE_RATE_LIMITED);

	      signedurl = oauth_sign_url2(myurl, &postargs, OA_HMAC, "POST", ckey, csecret, atok, atoksecret);
	      config_curlopts(curl, signedurl, postargs, (void *)&timeout);
	      break;
	    case 503:
	      fprintf(stderr, "Received HTTP error %d, attempting reconnect...\n", httpstatus);
	      reconnect_wait(ERROR_TYPE_HTTP);
	      
	      signedurl = oauth_sign_url2(myurl, &postargs, OA_HMAC, "POST", ckey, csecret, atok, atoksecret);
	      config_curlopts(curl, signedurl, postargs, (void *)&timeout);
	      break;
	    default:
	      fprintf(stderr, "Unexpected HTTP error %d. Aborting...\n", httpstatus);
	      reconnect = 0;
	      break;
	    }
	  break;
	case CURLE_ABORTED_BY_CALLBACK:
	  print_error_to_console("Timeout, attempting reconnect...\n");
	  reconnect_wait(ERROR_TYPE_SOCKET);
	  
	  signedurl = oauth_sign_url2(myurl, &postargs, OA_HMAC, "POST", ckey, csecret, atok, atoksecret);
	  config_curlopts(curl, signedurl, postargs, (void *)&timeout);
	  break;
	default:
	  // Probably a socket error, attempt reconnnect
	  print_error_to_console("Unexpected error, attempting reconnect...\n");
	  reconnect_wait(ERROR_TYPE_SOCKET);
	  curl_easy_cleanup(curl);
	  curl = curl_easy_init();
	  signedurl = oauth_sign_url2(myurl, &postargs, OA_HMAC, "POST", ckey, csecret, atok, atoksecret);
	  config_curlopts(curl, signedurl, postargs, (void *)&timeout);
	  break;
	}
    }

  printf("Cleaning up...\n");
  curl_easy_cleanup(curl);
  curl_global_cleanup();
  
  free(ckey);
  free(csecret);
  free(atok);
  free(atoksecret);
  free(signedurl);
  free(url);
  
  return 0;
}

/* config_curlopts
 * curl: cURL easy handle
 * url: URL of streaming endpoint
 * postargs: POST parameters
 * outfile: file stream for retrieved data
 * prog_data: data to send to progress callback function
 */
void config_curlopts(CURL *curl, const char *url, char *postargs, void *prog_data)
{
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, "tweetslurp/0.1");

	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postargs);

	// libcurl will now fail on an HTTP error (>=400)
	curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);

	/* write callback by Stenberg START */
	/* send all data to this function  */
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
	/* we pass our 'chunk' struct to the callback function */
	/* curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk); */
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);	
	/* write callback by Stenberg STOP */

	// noprogress must be set to 0 for a user-defined
	// progress method to be called
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);

	curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, progress_callback);
	curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, (void *)prog_data);
}

/* reconnect_wait
 * error: type of error we're recovering from
 */
void reconnect_wait(error_type error)
{
  static int http_sleep_s = 5;
  static int rate_limit_sleep_s = 60;
  static long sock_sleep_ms = 250;

  struct timespec t;
  switch(error)
    {
    case ERROR_TYPE_HTTP:
      t.tv_sec = http_sleep_s;
      t.tv_nsec = 0;

      // As per the streaming endpoint guidelines, double the
      // delay until 320 seconds is reached
      http_sleep_s *= 2;
      if(http_sleep_s > 320)
	{
	  http_sleep_s = 320;
	}
      break;
    case ERROR_TYPE_RATE_LIMITED:
      t.tv_sec = rate_limit_sleep_s;
      t.tv_nsec = 0;
      
      // As per the streaming endpoint guidelines, double the
      // delay
      rate_limit_sleep_s *= 2;
      break;
    case ERROR_TYPE_SOCKET:
      t.tv_sec = sock_sleep_ms / 1000;
      t.tv_nsec = (sock_sleep_ms % 1000) * MS_TO_NS;
      
      // As per the streaming endpoint guidelines, add 250ms
      // for each successive attempt until 16 seconds is reached
      sock_sleep_ms += 250;
      if(sock_sleep_ms > 16000)
	{
	  sock_sleep_ms = 16000;
	}
      break;
    default:
      t.tv_sec = 0;
      t.tv_nsec = 0;
      break;
    }
  nanosleep(&t, NULL);
}

/* progress_callback
 * see libcURL docs for method sig details
 */
int progress_callback(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow)
{
  struct idletimer *timeout;
  timeout = (struct idletimer *)clientp;
  
  if(dlnow == 0) // No data was transferred this time...
    {
      // ...but some was last time:
      if(timeout->lastdl != 0)
	{
	  // so start the timer
	  timeout->idlestart = time(NULL);
	}
      // ...and 1) the timer has been started, and
      // 2) we've hit the timeout:
      else if(timeout->idlestart != 0 &&
	      (time(NULL) - timeout->idlestart) > DATA_TIMEOUT)
	{
	  // so we reset the timer and return a non-zero
	  // value to abort the transfer
	  timeout->lastdl = 1;
	  timeout->idlestart = 0;
	  return 1;
	}
    }
  else // We transferred some data...
    {
      // ...but we didn't last time:
      if(timeout->lastdl == 0)
	{
	  // so reset the timer
	  timeout->idlestart = 0;
	}
    }
  
  timeout->lastdl = dlnow;
  return 0;
}
